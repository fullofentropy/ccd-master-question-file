"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  A0561 - Demonstrate the ability to create, reuse and import modules
  S0382 - Create a class constructor or destructor
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0066 - Add and remove nodes from a Linked List.
  S0065 - Find an item in a Linked List.
  S0052 - Implement a function that returns a single value.
  S0055 - Create and destroy a Linked List.
  S0048 - Implement a function that receives input parameters.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
'''
Task #1

Write a module called myNode that contains a class called Node with the following
instance attributes defined in the class' constructor:
    self.num   (stores an Integer)
    self.next  (reference to another Node object)

Task #2

Import the Node class from your new module to use in this file.

Write the function processNames that receives a tuple of strings ("names"). The
tuple contains peoples' first names. The processNames function should 
create a linked list using the Node class defined in your module.
The processNames function should iterate the tuple and insert the names into nodes 
so they are in ascending (alphabetical) order in the linked list.

If a name in the tuple is already in the linked list (duplicate), then the node 
containing that name should be removed from the linked list so that name does not exist in the linked list.

The processNames function should return a reference to the head node of the link list once processed.

'''
class Node:
    def __init__(self, name):
        self.name = name
        self.next = None

def buildList(names):
    head = None
       
    return head
