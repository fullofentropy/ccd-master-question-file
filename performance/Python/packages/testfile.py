"""
This question is intended to evaluate the following topics:
  S0027 - Install for a Python package using PIP.
  S0028 - Set up and replicate a Python Virtual Environment.
  S0024 - Declare and/or implement container data type.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input."""
"""
For this problem you will need to create a virtual environment and install the python package astar_python inside of the virtual environment. After that is successful, you can begin writing your code.

Write a function navigate_world() that will use the A* algorithm in astar_python to traverse a multi dimensional array.
    The function will return the list of points from the start to the finish or none if there was no path to the finish

    The function needs to accept the following parameters:
     world: a 2 dimensional list containing integers
     start: a list containing the [col, row] to start
     end: a list containing the [col, row] to traverse to

Make sure the world size is at least 5x5; isn't a jagged array; and contains only 0, positive numbers, or None.
any invalid world configurations should return None.

"""


def navigate_world(world, start, finish):
    pass
