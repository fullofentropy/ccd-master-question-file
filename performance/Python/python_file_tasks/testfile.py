"""
This question is intended to evaluate the following topics:
  A0047 - Implement file management operations.
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0023 - Declare and implement data types.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0037 - Open and close an existing file.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0038 - Read, parse, write (append, insert, modify) file data.
  S0040 - Determine the size of a file.
  S0039 - Create and delete a file.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0080 - Demonstrate the skill to implement exception handling.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0110 - Implement error handling.
  S0082 - Implement conditional control flow constructs."""
'''
Write the function writeNewFile that has one parameter:

     fileName = the name of a text file to read. 
     
The function should determine and capture the size of the file in bytes.

The function will also read the file in "fileName" and write the contents of the file to a new
file. The name of the new file to write to should be the same name as the file
being read, prepended with the text "new". For example, if the name in "filename" 
is story.txt, the file that you write to should be named newstory.txt.

Each line of the file in "fileName" will contain a single sentence but some of the sentences
may contain one of the following errors:
   1. Some may not begin with a capital letter
   2. Some may be missing a period at the end.  
   
When writing to the new file, ensure all sentences begin with a capital letter 
and end with a period.  Ensure each sentence ends with a newline character '\n'
except for the last sentence in the file. Like so:

This is sentence one.\n
This is sentence two.\n
This is the last sentence.(EOF)

Once the new file is written, have the function delete the original file in 
"fileName".

The function will return the byte size of the original file.

If the file in "fileName" doesn't exist, the function should return -1 and not create
an output file. 
If the file is empty, the function should return -2 and not create an output file.

************************************************************************************************ 
* A copy of the original files to read are located in the original_files directory. You may    *
* make copies of these files and place in the working directory as needed to rerun your tests. *
************************************************************************************************


'''
import os


def writeNewFile(file_name):
    pass
