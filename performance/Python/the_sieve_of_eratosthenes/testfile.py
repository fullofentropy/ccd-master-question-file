"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0026 - Utilize standard library modules.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
"""
(The Sieve of Eratosthenes) A prime integer is any integer greater than 1 that is evenly divisible
only by itself and 1. The Sieve of Eratosthenes is a method of finding prime numbers. It operates
as follows:

    a) Create a list with all elements initialized to 1. List elements with prime subscripts
       will remain 1. All other list elements will eventually be set to zero.

    b) Starting with list element 2, every time a list element is found whose value is 1, loop
       through the remainder of the list and set to zero every element whose subscript is a multiple
       of the subscript for the element with value 1. For list subscript 2, all elements beyond
       2 in the list that are multiples of 2 will be set to zero (subscripts 4, 6, 8, 10, etc.);
       for list subscript 3, all elements beyond 3 in the list that are multiples of 3 will be set to
       zero (subscripts 6, 9, 12, 15, etc.); and so on.

When this process is complete, the list elements that are still set to 1 indicate that the subscript is a
prime number. These subscripts can then be returned. Write a function sieve_of_Eratosthenes() that uses a list
of 1000 elements to determine and return the prime numbers between 2 and 999. Ignore element 0 of the list.

"""


def sieve_of_Eratosthenes():
    return ''







