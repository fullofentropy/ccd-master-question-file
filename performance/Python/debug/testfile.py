'''
A school coverts each test score grade to a GPA using the scale below.  When the term is over
they compute the average of all GPAs to determine an overall GPA and grade for the course.

Grade	Score	GPA  
A		93-100	4
A-		90-92	3.7
B+		87-89	3.3
B		83-86	3
B-		80-82	2.7
C+		77-79	2.3
C		73-76	2
C-		70-72	1.7
D+		67-69	1.3
D		65-66	1
F   	< 65    0

The computeGPA function receives a list of test scores and should:
    1. Compute a GPA for each test
	2. Throw out the two lowest GPAs
	3  Find the overall average GPA for all the GPAs.  
	4. Then, based on the overall GPA average, compute a final grade.
	5. Return the average GPA rounded to two decimal positions along with the final grade.
	6. If any score in the list is less than 0 or greater than 100, the function returns "INVALID SCORE"

However there are several syntax and logical errors in the code below that neeed to be identified and
corrected before this function executes properly.

'''


def computeGPA(scoreList):
    GPAs = []
    avg = 0
    for score in scorelist:            
        if score < 0 and score > 100:  
            return "INVALID SCORE"
         if score > 93:                
            GPAs.append(4)
        elif score >= 90:
            GPAs.append(3.7)
        elif score >= 87:
            GPAs.append(3.3)
        elif score >= 83:
            GPAs.append(3)
        elif score >= 80:
            GPAs.append(2.7)
        elif score >= 77:
            GPAs.append(2)        
        elif score >= 73:
            GPAs.append(2.3)      
        elif score >= 70:
            GPAs.append(1.7)
        elif score >= 67:
            GPAs.append(1.3)
        elif score >= 65:
            GPAs.append(1)
        else                     
            GPAs.append(0)
        
    GPAs.sort()                  
    GPAs.pop()					 
	GPAs.pop()


    avg = avg/len(GPAs)         

    for gpa in GPAs:
        avg + gpa              
    
    finalGrade = ' '
    
    if avg <= 4:              
        finalGrade = 'A'
    elif avg >= 3.7:             
        finalGrade = 'A-'
    elif avg >= 3.3:
        finalGrade = 'B+'
    elif avg >= 3:
        finalGrade = 'B'
    elif avg >= 2.7:
        finalGrade = 'B-'
    elif avg >= 2.3:
        finalGrade = 'C+'
    elif avg >=2:
        finalGrade = 'C'
    elif avg >= 1.7:
        finalGrade = 'C-'
    elif avg >= 1.3:
        finalGrade = 'D+'
    elif avg >= 1:
        finalGrade = 'D'
    else:
        finalGrade = 'F'
    
    return float(avg), finalGrade   
