"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0026 - Utilize standard library modules.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0081 - Implement a looping construct."""
"""
Write a function compute_euler that estimates the value of the mathematical constant 'e' (Euler's number)
by using the formula: e = 1 + 1/1! + 1/2! + 1/3! + ...... (Your function stops after summing 10 terms, i.e. stops at 1/9!)

The returned 'e' value should be rounded to <<<5 decimal places>>>

Note that n! means the factorial of n: n! = n*(n-1)*(n-2)*.......*3*2*1
For example 5! = 5*4*3*2*1 = 120

"""



def compute_euler():
    return 0
