import unittest, xmlrunner
from testfile import *


class CheckRideTestOne(unittest.TestCase):
	def test_files(self):
		good = [['bbergner', 46], ['rboone', 44], ['ecruz', 44], ['dfarr', 42], ['jwarren', 38]]
		small = [['ecruz', 44], ['rboone', 34], ['dgilles', 26], ['jwarren', 14]]
		
		self.assertEqual(good, find_culprits("mainList.txt"))
		self.assertEqual(small, find_culprits("smallList.txt"))
		
	def test_bad_file(self):
		self.assertEqual("BAD_FILE", find_culprits("bad.txt"))

	def test_empty(self):
		self.assertEqual([], find_culprits("empty.txt"))

		  
if __name__ == '__main__':
	with open('unittest.xml', 'w') as output:
		unittest.main(
		testRunner=xmlrunner.XMLTestRunner(output=output),
		failfast=False,
		buffer=False,
		catchbreak=False
		)





