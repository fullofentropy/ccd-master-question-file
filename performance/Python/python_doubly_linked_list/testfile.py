"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0067 - Find an item in a Doubly Linked List.
  S0068 - Add and remove nodes from a Doubly Linked List.
  S0052 - Implement a function that returns a single value.
  S0056 - Create and destroy a Doubly Linked List.
  S0048 - Implement a function that receives input parameters.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
'''
Task #1
    Write the function buildList that receives one parameter:
        names - a list of strings of people's first names

    The buildList function should iterate through the "names" list and create a doubly-linked list
    creating a node for each name in the list. Use the Node class defined below to create the doubly-linked list.
    Each name is added to the head of the doubly-linked list.

    The function should return a reference to the head node of the doubly-linked list
    when complete.

Task #2

    Write the function removeName that receives two parameters:
        head - a reference to the head node of the doubly-linked list created in Task #1
        findName - a string of a person's name that may be in a linked-list

    This function will search the doubly-linked list for the name in "findName".
    If found, the node containing the name must be removed from the doubly-linked list.
    If not found, do nothing.

    The function should return a reference to the head node of the doubly-linked list
    when complete.

Task #3

    Write the function appendName that receives two parameters:
        head - a reference to the head node of the doubly-linked list created in Task #2
        addName - a string of a person's name that will be added to the linked list

    This function should create a new Node with the name in "addName" and append that node to the end
    of the doubly-linked list.

    The function should return a reference to the head node of the doubly-linked list
    when complete.

'''


class Node:
    def __init__(self, name):
        self.name = name
        self.next = None
        self.prev = None


def buildList(names):

    return head


def removeName(head, findName):

    return head


def appendName(head, addName):

    return head
