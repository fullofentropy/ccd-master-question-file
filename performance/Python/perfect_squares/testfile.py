"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0026 - Utilize standard library modules.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0047 - Implement a function that returns multiple values.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
'''
Perfect squares are numbers that are created when you take a whole number times itself.
Ex: 4 is a perfect square because 2 times 2 is 4.

This problem entails 3 phases:
The findPerfectSquares function will be provided a list of numbers. 
1) The function needs to determine which of these numbers are perfect squares.
2) Compute the square roots of those numbers.
3) And finally place the integer values of the square roots in a new list. 

The function should return two items: 
1) The sorted (ascending) list of square roots For example, if the function is provided: [36,3,10,9,24,16,36]
2) The sum of all numbers in the square root list (in that order): [3,4,6,6], 19

If there are no perfect squares or the provided list is empty, return an empty list and 0.

DO NOT USE A LOOK-UP TABLE

'''


def findPerfectSquares(numbers):
    return 0, 0