import unittest, xmlrunner
from collections import Counter
from testfile import *

class TestTwo(unittest.TestCase):
    def test_find_matches(self):
        with open("test.log", "rt") as fp:
            input = fp.read()

        matches = find_matches(input)

        self.assertEqual(len(matches), 36)
        self.assertIn(("192.168.100.11", 59633), matches)
        self.assertIn(("192.168.0.11", 59633), matches)
        self.assertIn(("192.168.1.11", 59633), matches)
        self.assertIn(("10.0.10.252", 59633), matches)
        self.assertIn(("10.10.10.1", 52343), matches)
        self.assertNotIn(("192.168.100.11", 49486), matches)
        self.assertNotIn(("192.168.100.11", 63596), matches)

        counts = Counter(matches)
        self.assertEqual(counts[("192.168.100.11", 54068)], 15)
        self.assertEqual(counts[("192.168.100.11", 59633)], 13)


if __name__ == '__main__':
   #Uncomment the line below for stdout
   #unittest.main()

    with open('unittest.xml', 'w') as output:
      unittest.main(
      testRunner=xmlrunner.XMLTestRunner(output=output), 
      failfast=False, 
      buffer=False, 
      catchbreak=False
      )
