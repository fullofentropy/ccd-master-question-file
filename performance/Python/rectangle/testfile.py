"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0382 - Create a class constructor or destructor
  S0026 - Utilize standard library modules.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0110 - Implement error handling.
  S0082 - Implement conditional control flow constructs."""
"""
Create a Rectangle class that stores only the x-y coordinates of the upper left-hand and
lower right-hand corners of the rectangle.
The constructor calls a set function that accepts tuples of coordinates and verifies that each
of these is in the first quadrant, with no single x or y coordinate larger than 20.0.

The Rectangle class contains Methods (that can calls other utility methods) to calculate
the length, width, perimeter and area. The length is the larger of the two dimensions.
Include a predicate method isSquare that determines whether the rectangle is a square.

the class should contain the following methods:
    length()
    width()
    perimeter()
    area()
    isSquare()


"""



class Rectangle:
    pass
