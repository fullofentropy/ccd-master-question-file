/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0035 - Declare and/or implement of arrays and multi-dimensional arrays.
  S0033 - Utilize assignment operators to update a variable.
  S0042 - Open and close an existing file.
  S0043 - Read, parse, write (append, insert, modify) file data.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0090 - Allocate memory on the heap (malloc).
  S0097 - Create and use pointers.
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.
  S0156 - Utilize a struct composite data type.
  S0160 - Utilize the standard library.*/
#define _CRT_SECURE_NO_WARNINGS 1
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include"TestCode.h"
/*
In this program, you’ll design two structures for a person called BirthDate and HealthProfile. 

The BirthDate structure will include three integer members for the month, day and year of the birth.

The HealthProfile structure’s members will include: 
   - two character arrays for the person’s firstName and lastName, allowing for 14 characters each;
   - a character for the person's gender;
   - a BirthDate structure for the person's dateOfBirth;
   - three integers representing the person's height (in inches), weight (in pounds) and maxHeartRate; and
   - three doubles representing the person's maxTargetRate, minTargetRate and BMI (Body Mass Index).

The HealthProfile needs to be defined before your program will compile.

Your program should have a function that reads the data (firstName, lastName, gender, dateOfBirth, height, weight) 
of 5 persons from a given file (PersonsData.txt) 
and uses it to set the members of a HealthProfile variable. 

The program also should include functions that calculate: 
   - the user’s age in years
   - maximum heart rate and target-heart-rate range, 
   - and body mass index (BMI). 

The formula for calculating your maximum heart rate in beats per minute is:
    maxHeartRate = 220 - your age in years. 

Your target heart rate is a range that’s 50–85% of your maximum heart rate:
     maxTargetRate = 0.85 * maxHeartRate;  
	 minTargetRate = 0.50 * maxHeartRate;  

The formulas for calculating BMI is 
   BMI = (weightInPounds × 703)/ (heightInInches × heightInInches)

 NOTES:
  - Round maxTargetRate to 2 decimal numbers
  - Round minTargetRate to 2 decimal numbers
  - Round BMI to 1 decimal number

The program will include a function: HealthProfile* computeHealthProfiles(int*& agesInYears)
The function will return the health profiles of all persons after the calculations of maximum heart rate,  maxTargetRate, minTargetRate, and BMI
The function also will receive an array of integers to return the ages of all users in years. 
Note that the ages of the persons will depend on the current date, so the array agesInYears will return the current ages of the persons. 

*/

HealthProfile healthDatabase[NUMBER_OF_PERSONS];

void readData()
{
	
}

int calculateAgeInYears(HealthProfile hp)
{

	return 0;
}
double calculateBMI(HealthProfile hp)
{

	return 0.0;
}

HealthProfile* computeHealthProfiles(int** agesInYears)
{
	
	return healthDatabase;
}
