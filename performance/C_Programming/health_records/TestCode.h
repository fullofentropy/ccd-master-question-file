#pragma once

#include <stdio.h>

#define NUMBER_OF_PERSONS 5
#define LINE_LENGTH 80



void readData();


#ifdef __cplusplus
extern "C" {
#endif
	// Task One
	HealthProfile* computeHealthProfiles(int** agesInYears);

#ifdef __cplusplus
}
#endif