/*
This question is intended to evaluate the following topics:
  A0087 - Create and implement a sort routine.
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0097 - Create and use pointers.
  S0108 - Utilize post and pre increment/decrement operators.
  S0098 - Implement a function pointer to call another function.
  S0391 - Create and utilize a function prototype*/
#define _CRT_SECURE_NO_WARNINGS 1


/*
Create a function, called sort(), that will sort an array of integers according to a function pointer that is passed into it as an input parameter.

The sort() function must accept an array of int, an integer specifying the length of the array, and a function pointer to a comparison function.
The sort function should not have any return values and must modify the input array. You may use any sorting algorithm to
sort the array, so long that it uses the aforementioned function pointers when comparing elements.

Create two comparison functions, one for ascending order and one for descending order. Each of these functions needs to compare
two elements and return a value back to the sort function.
 
Create two function pointers named:
	ascending
	descending

You must declare these function pointers in the header file. 
These function pointers must each point to their corresponding comparison function that sort can use.

The sort() function will run the comparison function to determine sorting order when comparing during the sort.
When sort is passed ascending, the function will modify the data set to be in ascending order e.g. 1, 2, 3, 4, 5
When passed descending, the data set will be modified in decending order e.g. 5, 4, 3, 2, 1
*/

