#include <gmock/gmock.h>
#include "TestCode.h"




TEST(TestCase1, encryptVigenereTest_cornerCases)
{
	char* plainTextList[] = {
							 NULL,
							 "",
							 "~!$23#-=/*?, < >",
							 " ",
							 "attack them tomorrow",
							 "ATTACK THEM TOMORROW"
	};

	ASSERT_EQ(NULL, encryptVigenere(plainTextList[0], "key"));
	ASSERT_EQ(NULL, encryptVigenere(plainTextList[1], "key"));
	ASSERT_EQ(0, strcmp("~!$23#-=/*?, < >", encryptVigenere(plainTextList[2], "key")));
	ASSERT_EQ(0, strcmp(" ", encryptVigenere(plainTextList[3], "key")));
	ASSERT_EQ(0, strcmp("attack them tomorrow", encryptVigenere(plainTextList[4], "a")));
	ASSERT_EQ(0, strcmp("attack them tomorrow", encryptVigenere(plainTextList[5], "a")));
	ASSERT_EQ(0, strcmp("attack them tomorrow", encryptVigenere(plainTextList[4], "A")));
	ASSERT_EQ(0, strcmp("attack them tomorrow", encryptVigenere(plainTextList[5], "A")));
}

TEST(TestCase1, encryptVigenereTest_normalCases)
{
	char* plainTextList[] = {
							 "launch the rocket",
							 " another secret message",
							 "A Third Secret Message! ",
							 "Message with number #123",
							 "we are discovered save yourself"
	};

	ASSERT_EQ(0, strcmp("tpmrep izi twrciv", encryptVigenere(plainTextList[0], "IPSEC")));
	ASSERT_EQ(0, strcmp(" icgxjmg kieztl qgahskg", encryptVigenere(plainTextList[1], "ipsec")));
	ASSERT_EQ(0, strcmp("i izmtl hwgtmi eiuapyi! ", encryptVigenere(plainTextList[2], "IPsec")));
	ASSERT_EQ(0, strcmp("utkwcot omvp cmqdmg #123", encryptVigenere(plainTextList[3], "IpSec")));
	ASSERT_EQ(0, strcmp("zi cvt wqngrzgvtw avzh cqyglmgj", encryptVigenere(plainTextList[4], "deceptive")));
}