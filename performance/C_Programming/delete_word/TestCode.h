#pragma once

#include <stdio.h>

#define ERROR_SUCCESS 0
#define MAX_PATH 260
#define ERROR_NOT_FOUND 1168
#define ERROR_INVALID_PARAMETER 87

#ifdef __cplusplus
extern "C" {
#endif
	// Task One
	int deleteWord(const char* sentence, const char* deleteMe);

#ifdef __cplusplus
}
#endif