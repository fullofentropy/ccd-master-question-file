/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0036 - Declare and implement a char * array (string).
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0097 - Create and use pointers.
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/*
A palindrome is a text phrase (excluding punctuation, spaces, and capitalization/case) 
that reads the same backwards or forwards. For example each of the following are palindromes: 
    Able was I ere I saw Elba.
	A nut for a jar of tuna
	Taco cat
	Was it a car or a cat I saw?
	Ed, I saw Harpo Marx ram Oprah W. aside.

Write a function palindrome that takes in a char * string of characters and determines whether it is a palindrome

if it is a palindrome, the function should return 1
if it is not a palindrome, the function should return 0
*/



int palindrome(char *phrase)
{
	   
	return 1;
}
