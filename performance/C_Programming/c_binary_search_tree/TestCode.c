/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0035 - Declare and/or implement of arrays and multi-dimensional arrays.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0049 - Implement a recursive function.
  S0074 - Find an item in a Binary Search Tree.
  S0075 - Add and remove nodes from a Binary Search Tree.
  S0052 - Implement a function that returns a single value.
  S0060 - Create and destroy a Binary Search Tree.
  S0053 - Implement a function that returns a memory reference.
  S0048 - Implement a function that receives input parameters.
  S0090 - Allocate memory on the heap (malloc).
  S0097 - Create and use pointers.
  S0091 - Unallocating memory from the heap (free).
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.
  S0156 - Utilize a struct composite data type.*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

/*

struct numNode
{
	int val;
	struct numNode *left;
	struct numNode *right;
};

A Binary Search Tree (BST) is a tree in which all the nodes follow the below-mentioned properties:
        The left sub-tree of a node has a value less than its parent node's value.
        The right sub-tree of a node has a value greater than its parent node's value.
        In this scenario, no duplicate values are allowed. And example BST is below

                              (root node)
                            77
                           /  \
                          /    \
                         /      \
                        /        \
                       54         89
                      /  \       /  \
                     /    \     /    \
                    NULL   61  NULL   95
                          /  \        / \
                         /    \      /   \
                        NULL  NULL  90   NULL
                                    / \
                                   /   \
                                  /     \
                                NULL    NULL
                             
Task #1

Write the function buildBST that receives an array of integers (nums) and an int representing the size
of the array. The function will build The BST by:

    1. Iterating through the array
    2. Using the provided numNode struct defined in TestCode.h (and shown above) and creating Nodes for each value
       in the array.
    3. Inserting each Node into the BST.

If a value in the array is already in the BST, ignore the value and continue processing the rest of 
the array.

When complete, the function will return a pointer to the root node of the BST.

Task #2

Write the recursive function destroyBST that receives a pointer to the root 
node of a BST. You must implement a recursive algorithm. The algorithm
will be manually verified by the evaluator.
The function will visit each node in the BST and free each node essentially
deleting all nodes in the BST. The function will return a count of the total
number of nodes freed.

*/

struct numNode* buildBST(int nums[], int size)
{
    return NULL;
}


int destroyBST(struct numNode *root)
{
    return 0;
}
