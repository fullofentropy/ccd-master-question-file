#pragma once

#include <stdio.h>

struct nameNode
{
	const char  *name;
	struct nameNode *next;
	struct nameNode *prev;
};


#ifdef __cplusplus
extern "C" {
#endif
    struct nameNode* buildList(const char**, int);
    struct nameNode* removeNode(struct nameNode*, const char*);
	void freeMemory(struct nameNode *);
#ifdef __cplusplus
}
#endif