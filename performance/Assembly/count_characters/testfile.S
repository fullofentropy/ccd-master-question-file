bits 32

section .text

global _counter
global _counter_loop
global _counter_compare
global _counter_increment

; 
; Returns the number of times a character occurs in a string.
;
; void __cdecl counter(char* x, char y)
_counter:
	push ebp
	mov ebp, esp

	sub esp, 0x14		; just some space

	; Stack addresses of interest
	; ebp + 0x08 original (input) string
	; ebp + 0x0C char to count

	; set up
	mov eax, [ebp+0x0C]	; address of the char to count
	mov ebx, [ebp+0x08] ; address of the string
	xor ecx, ecx		; clear ecx (counter)
	xor edx, edx		; clear edx (index)
	jmp _counter_loop

	;;;;;;;;;;;;
	;code begin

_counter_compare:
	mov ebx, [ebp+0x08]	; address of the string
	add ebx, edx		; go to correct char in the string
	push edx			; push edx to stack
	mov edx, [ebx]		; put this character in edx
	cmp al, dl			; do comparison
	pop edx				; get edx back off stack
	jnz _counter_increment
	inc ecx				; count a match

_counter_increment:
	inc edx				; increment to next position

_counter_loop:
	mov ebx, [ebp+0x08]	; address of the string
	add ebx, edx		; go to correct char in the string
	push edx			; push edx to stack
	mov edx, [ebx]		; put this character in edx
	test dl, dl			; do comparison for null
	pop edx				; get edx back
	jnz _counter_compare

	;code end
	;;;;;;;;;;;;

	mov eax, ecx	; put count of char in eax
	add esp, 0x14	; reverse space
	mov esp, ebp
	pop ebp

	ret
