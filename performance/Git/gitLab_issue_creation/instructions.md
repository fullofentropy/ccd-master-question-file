## This question is intended to evaluate the following topics:

- K0627 - User Story Development
- S0184 - Develop a user story from a requirement.
- S0384 - Create an issue.
- S0385 - Create a merge request.

# Using GitLab IDE

1. From your exam repository that was provided to you by the exam administrators:
    - Create an issue in GitLab in the form of a User Story that descibes the branch you are about to create and its purpose. 
    - Create acceptance criteria for the task.
    - Assign the issue to yourself.
2. Create a merge request from your new issue and take note of the branch being created.
3. After you have completed your exam and completed the steps in the Git command line problem:
    - Mark the merge request as ready.
    - Assign the merge request to your exam administrator.
