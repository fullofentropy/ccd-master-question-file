## This question is intended to evaluate the following topics:

- S0018 - Commit changes to a working branch.
- S0020 - Update a project
- S0022 - Clone a project
- S0386 - Checkout an existing branch.

# Using Git CLI

1. From the command line in your Exam Virtual Machine, create a clone of the Exam repository provided to you by the exam administrators.
2. Checkout the branch that you created in your GitLab merge request.
    - Take a screen shot of your current working branch name and save to your branch.
3. Commit and push your code often during your exam to minimize the possiblity of lost work.
4. Once your exam is complete:
    - Make a final commit with the message of `Final Submission`
    - Push your code to the remote branch.
    - Create a text file of your git log and save to your branch.
5. Return to GitLab problem for final step
