This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0047 - Implement a function that returns multiple values.
	S0096 - Utilize the Socket library.
	A0631 - IPv4 Addressing
	S0026 - Utilize standard library modules.
	S0110 - Implement error handling.
