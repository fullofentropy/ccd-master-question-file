"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  A0626 - String-based Protocol
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0096 - Utilize the Socket library."""
#!/usr/bin/env python

import socket, http.client, re

http_string = "GET / HTTP/1.1\r\n\r\n"
portpair = ('10.8.0.1', 5000) # webserver is running on port 5000 of the grader

g_user_id = 'User1'

"""
 The objective of this task is to utilize a relatively simple REST API provided by
 a webserver running on port 5000 of the grader system to obtain a key.

-> You will need to create two functions, each that handles a request.
   do_key(): /user/<user_id>/ url accepts a fictional HTTP "KEY" method, which will return a key value. This function 
             will return the key.
   do_put(): you will send the key value you received via an HTTP "PUT" method, /key/<user_id>/<key_val>/. This 
             function will return the response.

"""


def do_key():
    pass

def do_put(key_val):
    pass
