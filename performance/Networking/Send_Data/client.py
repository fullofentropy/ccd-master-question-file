"""
This question is intended to evaluate the following topics:
  A0117 - Develop client/server network programs.
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  A0626 - String-based Protocol
  S0096 - Utilize the Socket library."""
import socket
from student_helper import *

"""
Student file: this is the file to modify

Set up communication using the python module socket.

The server has a simple string:
s = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'

You need to send it data_to_send in order to help it decrypt the string.
"""

portpair = ('10.8.0.1', 5000) # webserver is running on port 5000 of the grader

def send_the_data():
    data_to_send = serialize_dict_to_data(SCORES)
