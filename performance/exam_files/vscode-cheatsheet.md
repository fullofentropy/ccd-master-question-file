# VSCode Cheatsheet
**While in VSCode, render this file with `ctrl+shift+V`**

- Open a terminal: `ctrl+shift+~`
- Open debugger tab: `ctrl+shift+D`
- Render markdown file: `ctrl+shift+V` (while focus on file)
- Comment line: `ctrl+/` (while cursor on line)
- Open File: `ctrl+O`
- Open Folder: `ctrl+K` then `ctrl+O`

**Open keyboard shortcuts**
1. press `ctrl+shift+P`
2. type `keyboard shortcuts`
3. hit enter on `Preferences: Open Keyboard Shortcuts`

