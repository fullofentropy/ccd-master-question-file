# Git Instructions
**While in VSCode, render this file with `ctrl+shift+V`**

## 1. Setting up Git Config username and email
This will set the information that your `git commit` will be tagged with.
1. Open a VSCode terminal with `ctrl+shift+~`
2. Set your user.email with `git config --global user.email [EMAIL]`
    - use the same email as your GitLab account
3. Set your user.name with `git config --global user.name [FULL NAME]`
